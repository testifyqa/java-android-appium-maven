package steps;

import io.cucumber.java.en.When;
import pages.Page;
import pages.PopularTabPage;

public class NavigationSteps extends Page {

    @When("^the Reddit user views the Home tab$")
    public void iSelectTheHomeTab() {
        instanceOf(PopularTabPage.class).navToHomeTab();
    }
}