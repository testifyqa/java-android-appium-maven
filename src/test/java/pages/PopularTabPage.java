package pages;

import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static utils.extensions.MobileElementExtensions.meTap;

public class PopularTabPage extends BasePage {

    private static Logger log = LogManager.getLogger(PopularTabPage.class.getName());

    @AndroidFindBy(uiAutomator = "text(\"Home\")")
    public AndroidElement homeTab;

    public HomeTabPage navToHomeTab() {
        meTap(homeTab);
        log.info(":: We navigate to the 'Home' tab");
        return instanceOf(HomeTabPage.class);
    }
}