package pages;

import java.util.Arrays;
import java.util.List;

public class HomeTabPage extends PopularTabPage {

    public void assertIAmOnHomeTab() {
        validateCurrentActivity(".MainActivity");

        List<String> homeInfo = Arrays.asList("Welcome!", "Vote", "Subscribe", "Reddit community");
        validateMultipleInPageSource(homeInfo);
    }
}