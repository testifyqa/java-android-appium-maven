Feature: BaseScenarios
  These scenarios can be used in any project

  Scenario: 01. Validate the PageSource string on the app screen
    Then the Reddit user sees "Share, upvote, and discuss the best of the internet" in the PageSource

  Scenario: 02. Validate existence of multiple texts in PageSource
    Then the Reddit user sees
      | SKIP FOR NOW  |
      | LOG IN        |
      | SIGN UP       |

  Scenario: 03. Validate the Current Activity of the App
    Then the Reddit user sees the current activity is ".IntroductionActivity"

  Scenario: 04. Validate the Current Context of the App
    Then the Reddit user sees the current context is "NATIVE_APP"