@Nexus6pMarshmallow @Nexus5xOreo
Feature: Navigation Scenarios
  As a user of Reddit, I can navigate around the Reddit app

  Scenario: 01. View the 'Home' tab whilst logged out
    Given the welcome screen has been skipped without logging in
    When the Reddit user views the Home tab
    Then they see posts and information pertaining to the Home tab